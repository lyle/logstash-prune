# Manipulates logstash data from Elasticsearch

## Requirements

* Bash v2 or later
* [curl](https://curl.haxx.se/) for HTTP
* [jq](https://stedolan.github.io/jq/) for parsing JSON
* An IP address which is whitelisted to access the Elasticsearch index


## Running

	# TIER is one of "localhost", "test", "prod", or "red"
	./logprune TIER
